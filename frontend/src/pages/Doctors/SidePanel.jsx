// import React, { useState } from 'react';

// const Sidepanel = () => {
//   const [isAppointmentBooked, setAppointmentBooked] = useState(false);

//   const handleBookAppointment = () => {
//     // Simulate booking logic
//     // In a real application, you would have your booking API call or other logic here
//     // For simplicity, we'll just use a timeout to simulate an asynchronous operation
//     setAppointmentBooked(true);
//     setTimeout(() => {
//       setAppointmentBooked(true); // Reset the state after a few seconds
//     }, 3000); // Reset after 3 seconds (adjust as needed)
//   };

//   return (
//     <div className='shadow-panelshadow p-3 lg:p-5 rounded-md'>
//       <div className="flex items-center justify-between">
//         <p className="text_para mt-0 font-semibold">Ticket Price</p>
//         <span className="text-[16px] leading-7 lg:text-[22px] lg:leading-8 text-headingColor font-bold">
//           ₹ 500
//         </span>
//       </div>
//       <div className="mt-[30px]">
//         <p className="text_para mt-0 font-semibold text-headingColor">
//           Available Time Slots:
//         </p>
//         <ul className="mt-3">
//           {/* Time slot entries */}
//         </ul>
//       </div>
//       {isAppointmentBooked ? (
//         <p className="text-green-500 font-semibold mt-4">Appointment Booked! Thank you!</p>
//       ) : (
//         <button className='btn px-2 w-full rounded-md' onClick={handleBookAppointment}>
//           Book Appointment
//         </button>
//       )}
//     </div>
//   );
// };

// export default Sidepanel;

import React, { useState, useEffect } from 'react';

const Sidepanel = () => {
  const [isAppointmentBooked, setAppointmentBooked] = useState(false);
  const [bookingDetails, setBookingDetails] = useState(null);

  const handleBookAppointment = async () => {
    try {
      // Simulate booking logic
      // In a real application, you would have your booking API call or other logic here

      // Simulate getting additional details from the database after booking
      const detailsFromDatabase = await fetchBookingDetails(); // Assuming a function to fetch details
      setBookingDetails(detailsFromDatabase);

      setAppointmentBooked(true);
    } catch (error) {
      console.error('Error booking appointment:', error);
    }
  };

  const fetchBookingDetails = async () => {
    // Simulate fetching details from the database
    // In a real application, you would have your API call here
    const generatedAppointmentId = generateAppointmentId(); // Generate dynamically
    return {
      appointmentId: generatedAppointmentId,
      bookedTime: new Date().toLocaleString(), // Dynamic booked time
      // Add other details as needed
    };
  };

  const generateAppointmentId = () => {
    // Implement your logic to generate a dynamic appointment ID
    // For simplicity, this example uses a random number
    return Math.floor(Math.random() * 10000).toString();
  };

  useEffect(() => {
    if (isAppointmentBooked) {
      const resetBookingState = setTimeout(() => {
        setAppointmentBooked(true);
        setBookingDetails(null);
      }, 500000); // Reset after 5 seconds (adjust as needed)

      return () => clearTimeout(resetBookingState); // Cleanup on component unmount
    }
  }, [isAppointmentBooked]);

  return (
    <div className='shadow-panelshadow p-3 lg:p-5 rounded-md'>
      <div className="flex items-center justify-between">
        <p className="text_para mt-0 font-semibold">Ticket Price</p>
        <span className="text-[16px] leading-7 lg:text-[22px] lg:leading-8 text-headingColor font-bold">
          ₹ 500
        </span>
      </div>
      <div className="mt-[30px]">
        <p className="text_para mt-0 font-semibold text-headingColor">
          Available Time Slots:
        </p>
        <ul className="mt-3">
          {/* Time slot entries */}
        </ul>
      </div>
      {isAppointmentBooked ? (
        <>
          <p className="text-green-500 font-semibold mt-4">Appointment Booked! Thank you!</p>
          {bookingDetails && (
            <div className="mt-4">
              <p className="text-headingColor font-semibold">Booking Details:</p>
              <p>Appointment ID: {bookingDetails.appointmentId}</p>
              <p>Booked Time: {bookingDetails.bookedTime}</p>
              {/* Add other details as needed */}
            </div>
          )}
        </>
      ) : (
        <button className={`btn px-2 w-full rounded-md ${isAppointmentBooked && 'hidden'}`} onClick={handleBookAppointment}>
          Book Appointment
        </button>
      )}
    </div>
  );
};

export default Sidepanel;
