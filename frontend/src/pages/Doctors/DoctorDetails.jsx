// import React, { useState, useEffect } from "react";
// import { useParams } from 'react-router-dom';
// import doctorImg from "../../assets/images/doctor-img02.png";
// import starIcon from '../../assets/images/Star (1).png';
// import DoctorAbout from "./DoctorAbout";
// import Feedback from "./Feedback";
// import Sidepanel from "./Sidepanel";
// import DoctorDerm from "./DoctorDerm";
// import DoctorNeur from "./DoctorNeur";

// const DoctorDetails = () => {
//   const { doctorId } = useParams();
//   const [tab, setTab] = useState('about');
//   const [selectedDoctor, setSelectedDoctor] = useState(null);

//   // Define a mapping of doctorId to corresponding components
//   const doctorComponents = {
//     '1': {
//       about: tab === 'about' && <DoctorAbout />,
//       feedback: tab === 'feedback' && <Feedback />,
//     },
//     '2': {
//       about: tab === 'about' && <DoctorDerm />,
//       feedback: tab === 'feedback' && <Feedback />,
//     },
//     '3': {
//       about: tab === 'about' && <DoctorNeur />,
//       feedback: tab === 'feedback' && <Feedback />,
//     },
//   };

//   useEffect(() => {
//     // Set the selected doctor's details when the doctorId changes
//     setSelectedDoctor(doctorComponents[doctorId] || null);
//   }, [doctorId, tab]);

//   return (
//     <section>
//       <div className="max-w-[1170px] px-5 mx-auto">
//         <div className="grid md:grid-cols-3 gap-[50px]">
//           <div className="md:col-span-2">
//             <div className="flex items-center gap-5">
//               {/* ... Doctor details rendering code ... */}
//             </div>

//             <div className="mt-[50px] border-b border-solid border-[#0066ff34]">
//               <button
//                 onClick={() => setTab('about')}
//                 className={`${tab === 'about' && 'border-b border-solid border-primaryColor'} py-2 px-5 mr-5 text-[16px] leading-7 text-headingColor font-semibold`}
//               >
//                 About
//               </button>
//               <button
//                 onClick={() => setTab('feedback')}
//                 className={`${tab === 'feedback' && 'border-b border-solid border-primaryColor '} py-2 px-5 mr-5 text-[16px] leading-7 text-headingColor font-semibold`}
//               >
//                 Feedback
//               </button>
//             </div>

//             <div className="mt-[50px]">
//               {/* Display the selected doctor's components based on the tab */}
//               {selectedDoctor?.about}
//               {selectedDoctor?.feedback}
//             </div>
//           </div>

//           <div>
//             <Sidepanel />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };

// export default DoctorDetails;

















import React, { useState } from "react";
import { useParams } from 'react-router-dom';
import doctorImg from "../../assets/images/doctor-img02.png";
import starIcon from '../../assets/images/Star (1).png'
import DoctorAbout from "./DoctorAbout";
import Feedback from "./Feedback";
import Sidepanel from "./Sidepanel";
import DoctorDerm from "./DoctorDerm";
import DoctorNeur from "./DoctorNeur";
const DoctorDetails = () => {
  const { doctorId } = useParams();
  const [tab, setTab] = useState('about')
  return (
    <section>
      <div className="max-w-[1170px] px-5 mx-auto">
        <div className="grid md:grid-cols-3 gap-[50px]">
          <div className="md:col-span-2">
            <div className="flex items-center gap-5">
              <figure className="max-w-[200px] max-h-[200px]">
                <img src={doctorImg} alt="" className="w-full" />
              </figure>

              <div>
                <span className="bg-[#CCF0F3] text-irisBlueColor py-1 px-6 lg:py-2 lg:px-6 text-[12px] leading-4 lg:text-[16px] lg:leading-7 font-semibold rounded">
                Neurologist
                </span>
                <h3 className="text-heading Color text-[22px] leading-9 mt-3 font-bold">
                Saleh Mahmud
                </h3>
                <div className="flex items-center gap-[6px]">
                  <span className="flex items-center gap-[6px] text-[14px] leading-5 lg:text-[16px] lg:leading-7 font-semibold text-headingColor">
                    <img src={starIcon} alt="" /> 4.8
                  </span>
                  <span className="text-[14px] leading-5 lg: text-[16px] lg:leading-7 font-[400] text-textColor">
                    (272)
                  </span>
                </div>
                <p className="text_para text-[14px] leading-6 md:text-[15px] lg:max-w-[390px]"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta, alias!</p>
              </div>

            </div>
            <div className="mt-[50px] border-b border-solid border-[#0066ff34]">
              <button
                onClick={() => setTab('about')}
                className={`${tab === 'about' && 'border-b border-solid border-primaryColor'} py-2 px-5 mr-5 text-[16px] leading-7 text-headingColor font-semibold`}
              >
                About
              </button>
              <button
                onClick={() => setTab('feedback')}
                className={`${tab === 'feedback' && 'border-b border-solid border-primaryColor '} py-2 px-5 mr-5 text-[16px] leading-7 text-headingColor font-semibold`}
              >
                Feedback

              </button>
            </div>

           

            <div className="mt-[50px">
              {tab === 'about' && <DoctorAbout />}
              {tab === 'feedback' && <Feedback />}
              
            </div>

<div className="mt-[50px]">
              {doctorId === '1' && tab === 'about' && <DoctorAbout />}
              {doctorId === '1' && tab === 'feedback' && <Feedback />}
              
              {doctorId === '2' && tab === 'about' && <DoctorDerm />}
              {doctorId === '2' && tab === 'feedback' && <Feedback />}

              {doctorId === '3' && tab === 'about' && <DoctorNeur />}
              {doctorId === '3' && tab === 'feedback' && <Feedback />}
            </div>

          </div>

          <div>
            <Sidepanel />
          </div>
          {/* <div>
              <DoctorDerm />
            </div>

            <div>
              <DoctorNeur />
            </div> */}
        </div>
      </div>
    </section>
  )
}

export default DoctorDetails
