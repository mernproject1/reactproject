

// const Contact = () => {
//   return (
//     <section>
//       <div className="px-4 mx-auto max-w-screen-md ">
//         <h2 className="heading text-center">Contact Us</h2>
//         <p className="mb-8 lg:mb-16 font-light text-center text__para">
//           Got a technical issue? Want to send feedback about a beta feature? Let us know.
//         </p>
//         <form action="#" className="space-y-8">
//           <div>
//             <label htmlFor="email" className="form__label">
//               Your Email
//             </label> 
//             <input
//               type="email"
//               id="email"
//               placeholder="example@gmail.com"
//               className="form__input mt-1" />
//           </div>
//           <div>
//             <label htmlFor="subject" className="form__label">
//               subject
//             </label> 
//             <input
//               type="text"
//               id="subject"
//               placeholder="Let us know how we can help you"
//               className="form__input mt-1" />
//           </div>
//           <div  className="sm:col-span-2 ">
//             <label htmlFor="message" className="form__label">
//               Your Message
//             </label> 
//             <textarea
//             rows='6'
//               type="email"
//               id="message"
//               placeholder="Leave a comment"
//               className="form__input mt-1" />
//           </div>
//           <button type="submit" className="btn rounded sm:w-fit" >Submit</button>
//         </form>
//       </div>
//     </section>
//   );
// };
// export default Contact




import React, { useState } from 'react';

const Contact = () => {
  const [formSubmitted, setFormSubmitted] = useState(false);
  const [email, setEmail] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    // Check if all fields are filled before considering it a successful submission
    if (email && subject && message) {
      // Add your form submission logic here, such as sending data to a server.
      // For now, just simulate a successful submission.
      setFormSubmitted(true);
    }
  };

  return (
    <section>
      <div className="px-4 mx-auto max-w-screen-md ">
        <h2 className="heading text-center">Contact Us</h2>
        {!formSubmitted ? (
          <form onSubmit={handleSubmit} className="space-y-8">
            <div>
              <label htmlFor="email" className="form__label">
                Your Email
              </label>
              <input
                type="email"
                id="email"
                placeholder="example@gmail.com"
                className="form__input mt-1"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div>
              <label htmlFor="subject" className="form__label">
                Subject
              </label>
              <input
                type="text"
                id="subject"
                placeholder="Let us know how we can help you"
                className="form__input mt-1"
                value={subject}
                onChange={(e) => setSubject(e.target.value)}
              />
            </div>
            <div className="sm:col-span-2">
              <label htmlFor="message" className="form__label">
                Your Message
              </label>
              <textarea
                rows="6"
                id="message"
                placeholder="Leave a comment"
                className="form__input mt-1"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
              />
            </div>
            <button type="submit" className="btn rounded sm:w-fit">
              Submit
            </button>
          </form>
        ) : (
          <div className="text-center">
            <p className="text-green-500">Success! Your message has been submitted.</p>
          </div>
        )}
      </div>
    </section>
  );
};

export default Contact;
