// Controllers/appointmentController.js

import Appointment from '../Routes/Appointment.js';

// Named export
export const bookAppointment = async (req, res) => {
  try {
    const newAppointment = new Appointment();
    await newAppointment.save();
    res.status(201).json({ message: 'Appointment booked successfully!' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};
