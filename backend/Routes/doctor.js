import express from 'express';
import { updateDoctor
    ,deleteDoctor
    ,getAllDoctor
    ,getSingleDoctor,
    getDoctorProfile
 } from '../Controllers/doctorController.js';
import { authenticate, restrict} from '../auth/verifyToken.js';

import reviewRouter from './review.js'

const router = express.Router()

//nested route
router.use('/:doctorId/reviews', reviewRouter)
router.get('/:id', getSingleDoctor)
router.get('/', getAllDoctor)
router.put('/:id', authenticate, restrict(["doctor"]), updateDoctor)
router.delete('/:id', authenticate, restrict(["doctor"]), deleteDoctor)
router.get('/profile/me', authenticate, restrict(["doctor"]), getDoctorProfile)

router.post('/:doctorId/appointments', async (req, res) => {
    try {
      const { doctorId } = req.params;
      const { user, appointmentDate } = req.body;
  
      console.log('Received appointment request:', { doctorId, user, appointmentDate });
  
      // Validate if the doctor exists
      const doctor = await Doctor.findById(doctorId);
      if (!doctor) {
        console.log('Doctor not found');
        return res.status(404).json({ message: 'Doctor not found' });
      }
  
      // Create a new appointment
      const appointment = new Appointment({
        doctor: doctorId,
        user,
        appointmentDate,
      });
  
      // Save the appointment to the database
      await appointment.save();
  
      console.log('Appointment created successfully');
      res.status(201).json({ message: 'Appointment created successfully' });
    } catch (error) {
      console.error('Error creating appointment:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  });


export default router;