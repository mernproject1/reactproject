import express from "express";
import cookieParser from "cookie-parser"; 
import cors from "cors"; 
import mongoose from "mongoose"; 
import dotenv from "dotenv"; 
import authRoute from './Routes/auth.js';
 import userRoute from './Routes/user.js';
 import doctorRoute from './Routes/doctor.js';
 import reviewRoute from './Routes/review.js';
 import appointmentRoute from "./Routes/Appointment.js";
 import bodyParser from "body-parser";
//  import appointmentController from "./Controllers/appointmentController.js";
//  import { bookAppointment } from "./Controllers/appointmentController.js";

//  const bodyParser = require('body-parser');
//  const appoint = require('./Controllers/appointmentController.js');
dotenv.config()

const app = express();
const port = process.env.PORT || 8000
app.use(bodyParser.json());
mongoose.connect('mongodb+srv://mahendergundaboina:Ap8afm0LR8wGFTLQ@cluster0.bqyrmrv.mongodb.net/?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true });
// app.post('/api/bookAppointment', appointmentController.bookAppointment);

const corsOptions = {
origin: true
};
app.get("/", (req, res)=> {
res.send('Api is working')
});

//database connection
mongoose.set('strictQuery', false)
const connectDB = async()=>{
   try{
      await mongoose.connect(process.env.MONGO_URL, {
         useNewUrlParser: true,
         useUnifiedTopology: true,
      });

      console.log('MongoDB database is connected ');
   } catch(err){
      console.log('MongoDB database connection failed');
   }
}

//middleware
app.use(express.json());
app.use(cookieParser());
app.use(cors(corsOptions));
app.use('/api/v1/auth', authRoute);
 app.use('/api/v1/users', userRoute);
 app.use('/api/v1/doctors', doctorRoute);
 app.use('/api/v1/reviews', reviewRoute);
app.use('/api/v1/appointments',appointmentRoute);
// app.use('/api/v1/appoint',appointmentRoute);
// app.use('/api/v1/appointmentController',appointmentController);
// app.post('/api/bookAppointment', bookAppointment);
app.listen(port, ()=>{
    connectDB()
    console.log('Server is Running on port' + ' ' + port);
});



// import express from "express";
// import cookieParser from "cookie-parser";
// import cors from "cors";
// import mongoose from "mongoose";
// import dotenv from "dotenv";
// import authRoute from './Routes/auth.js';
// import userRoute from './Routes/user.js';
// import doctorRoute from './Routes/doctor.js';
// import reviewRoute from './Routes/review.js';
// import appointmentRoute from "./models/Appointment.js";
// import { bookAppointment } from "./Controllers/appointmentController.js";

// dotenv.config();

// const app = express();
// const port = process.env.PORT || 8000;

// app.use(express.json());
// app.use(cookieParser());
// app.use(cors());

// mongoose.connect('mongodb+srv://mahendergundaboina:Ap8afm0LR8wGFTLQ@cluster0.bqyrmrv.mongodb.net/?retryWrites=true&w=majority', {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// });

// app.get("/", (req, res) => {
//   res.send('Api is working');
// });

// const connectDB = async () => {
//   try {
//     await mongoose.connect(process.env.MONGO_URL, {
//       useNewUrlParser: true,
//       useUnifiedTopology: true,
//     });

//     console.log('MongoDB database is connected ');
//   } catch (err) {
//     console.log('MongoDB database connection failed');
//   }
// };

// app.use('/api/v1/auth', authRoute);
// app.use('/api/v1/users', userRoute);
// app.use('/api/v1/doctors', doctorRoute);
// app.use('/api/v1/reviews', reviewRoute);
// app.use('/api/v1/appointments', appointmentRoute);
// app.use('/api/v1/appoint', appointmentRoute);

// app.post('/api/bookAppointment', bookAppointment);

// app.listen(port, () => {
//   connectDB();
//   console.log('Server is Running on port' + ' ' + port);
// });
